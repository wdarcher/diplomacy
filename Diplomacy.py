from io import StringIO


def diplomacy_read(s):
    a = []
    s = s.read()
    # splits the file into a list split at new line characters
    a = s.split('\n')

    # left in case there was an extra new line character or a space following the new line
    if a[-1] == '':
        a = a[:-1]

    return a

def diplomacy_print(w, out): 
    """
    w a writer
    out a list of tuples
    writes the tuple values split by a space and followed by a new line
    """
    for _ in out:
        w.write(' '.join(_) + "\n")

def diplomacy_solve(r, w): 
    """
    r a reader
    w a writer
    """
    i = diplomacy_read(r)
    v = diplomacy_eval(i)
    diplomacy_print(w, v)


def diplomacy_eval(armies):
    # split each element at spaces for organization of army name, location, and action
    for i in range(len(armies)):
            armies[i] = armies[i].split(' ')
    all_locations = {}
    supporting = []
    # iterates through armies
    # adds class Army to dictionary of all locations
    for i in armies:
        # adds supporting armies to a list called supporting to deal with the support count following all armies added to dictionary 
        if i[2] == 'Support':
            if i[1] in all_locations.keys():
                all_locations[i[1]].append(Army(name=i[0], location=i[1]))
                
            else:
                all_locations[i[1]] = [Army(name=i[0], location=i[1])]
            supporting.append((i[0], i[1], i[3]))
        else:
            if i[2] == 'Move':
                if i[3] in all_locations.keys():
                    all_locations[i[3]].append(Army(name=i[0], location=i[3]))
                else:
                    all_locations[i[3]] = [Army(name=i[0], location=i[3])]
                
            elif i[2] == 'Hold':
                if i[1] in all_locations.keys():
                    all_locations[i[1]].append(Army(name=i[0], location=i[1]))
                else:
                    all_locations[i[1]] = [Army(name=i[0], location=i[1])]

    # iterate through supporting and adds support to armies if armies support is valid
    for s in supporting:
        found = False
        if len(all_locations[s[1]]) < 2:
            for k in all_locations:
                if found == True:
                    break
                for j in all_locations[k]:
                    if j.name == s[2]:
                        j.incrementSupport()
                        found = True
                        break

    # checks status of armies by iterating over dict of all_locations
    finalList = []
    for i in all_locations:
        if len(all_locations[i]) > 1:
            resp = checkWins(all_locations[i])
            if resp[0] == True:
                for j in all_locations[i]:
                    finalList.append((j.name, '[dead]'))
            else:
                for j in all_locations[i]:
                    if j.support < resp[1]:
                        finalList.append((j.name, '[dead]'))
                    else:
                        finalList.append((j.name, j.location))
        else:
            finalList.append((all_locations[i][0].name, all_locations[i][0].location))
    
    finalList = sorted(finalList)
    return finalList



def checkWins(ld):
    max = 0
    tie = False
    for i in ld:
        if i.support > max:
            max = i.support
            tie = False
        elif i.support == max:
            tie = True
    return (tie, max)
    


    



class Army:
    def __init__(self, name, location, support=0, dead=False):
        self.name = name
        self.location = location
        self.support = support
        self.dead = dead
        
    
    def incrementSupport(self):
        self.support = self.support + 1
    

if __name__ == '__main__': #pragma: no cover

    s = "C CorpusCristi Hold\nA Austin Move CorpusCristi\nG Georgia Move CorpusCristi\nS SanAntonio Support G\n"
    print(diplomacy_read(StringIO(s)))